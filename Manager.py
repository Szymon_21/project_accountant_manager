from modules.dekoratory import decorator
import sys

class Manager:

    def __init__(self):
        self.balance = 0
        self.assortment = {}   
        self.history = []
        self.load_data() 
        
    def load_data(self):
            with open('balance.txt') as balance:
                self.balance = int(balance.read())
                
            with open('history.txt') as history:
                for element in history.readlines():
                    self.history.append(element)

            try:
                self.history = [line.strip() for line in self.history]

            except:
                print('Coś poszło źle w dekoratorze.')

            with open('assortment.txt') as assortment:
                for element in assortment.readlines():
                    if len(element.split()) == 2:
                        self.assortment[element.split()[0]] = int(element.split()[1])

                    else:
                        key = ''
                        for index in range(len(element.split())-1):
                            key = key + str(element.split()[index]) + ' '
                        self.assortment[key[:-1]] = int(element.split()[-1])

    @decorator
    def saldo(self):
        self.price = int(input('Kwota: '))
        comment = input('Komentarz: ')
        self.balance += self.price
        self.type = 'Saldo'
        self.comment = comment
        return True

    @decorator
    def sprzedaz(self):
        self.id = input('ID produktu: ')
        self.price = int(input('Cena za sztukę: '))
        self.qty = int(input('Ilość sztuk: '))
        self.type = 'sprzedaz'

        try:
            if (self.price > 0 and self.qty > 0) and (self.assortment.get(self.id) >= self.qty):
                self.balance += self.price * self.qty
                self.assortment[self.id] -= self.qty
                self.history.append((f'Sprzedaż, ID Produktu: {self.id}, Kwota: {self.price}, qty: {self.qty}'))
                print(f'Sprzedaż, ID Produktu: {self.id}, Kwota: {self.price}, qty: {self.qty}')

            else:
                print('Wystąpił błąd. Upewnij się, że ilość i cena są prawidłowe.')

        except:
            print('Upewnij się, że produkt jest w magazynie.')
        return True

    @decorator
    def kupno(self):
        self.id = input('ID Produktu: ')
        self.price = int(input('Cena za sztukę: '))
        self.qty = int(input('Ilość sztuk: '))
        self.type = "Kupno"

        if (self.price * self.qty < self.balance) and (self.qty > 0 and self.price > 0):
            self.balance += -self.price * self.qty
            #print('RUB IF BUY')

            try:
                self.assortment[self.id] += self.qty

            except:
                self.assortment[self.id] = self.qty
            return True

        else:
            print('Wystąpił błąd. Spróbuj ponownie!')
            return False

mgr = Manager()
mgr.saldo()