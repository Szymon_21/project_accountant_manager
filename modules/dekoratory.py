def decorator(func):
        def inner(self):
            with open('balance.txt', 'w') as file:
                file.write(str(self.balance))
                print(f'balance: {self.balance}')

            if func(self):
                try:
                    with open('history.txt', 'a') as file:
                        file.write(f'{self.type}, Kwota: {self.price}, Opis: {self.comment}\n')
                        #print('RUN SALDO')

                except:
                    with open('history.txt', 'a') as file:
                        file.write(f'{self.type}, ID Produktu: {self.id}, Cena za sztukę: {self.price}, ilość sztuk: {self.qty}\n')

                    with open('assortment.txt', 'w') as file:
                        for line in self.assortment:
                            a = f'{line} {str(self.assortment[line])}\n'
                            file.write(a)
        return inner